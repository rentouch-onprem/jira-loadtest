import os
import json
import random
from locust import HttpUser, task, between
from requests_oauthlib import OAuth1
from requests_oauthlib.oauth1_session import SIGNATURE_RSA

PASSWORD = os.getenv("PASSWORD")
USERNAME = os.getenv("USERNAME", "load")
AUTH_METHOD = os.getenv("AUTH_METHOD")
available_methods = ("basic", "cookie", "oauth")
if AUTH_METHOD not in available_methods:
    print(
        f"[ERROR] Please set AUTH_METHOD env variable to a valid value. "
        f"Available are {available_methods}; current value is: {AUTH_METHOD}"
    )
    exit(1)


class GetProjectsUser(HttpUser):
    wait_time = between(1, 3)

    def on_start(self):
        if AUTH_METHOD == "basic":
            self.client.auth = (USERNAME, PASSWORD)
        elif AUTH_METHOD == "cookie":
            ret = self.client.post(
                "/rest/auth/1/session",
                json={"username": USERNAME, "password": PASSWORD},
            )
            session_response = ret.json()["session"]
            self.client.cookies[session_response["name"]] = session_response["value"]
        elif AUTH_METHOD == "oauth":
            try:
                with open("data/certs/jira_privatekey.pem") as f:
                    private_key = f.read()
            except Exception as e:
                print(f"[ERROR] Was not able to load private-key: {e}")
                exit(1)
            try:
                with open("data/oauth_creds.json") as f:
                    creds = json.load(f)
            except Exception as e:
                print(f"[ERROR] Was not able to read oauth credentials: {e}")
                exit(1)
            self.client.auth = OAuth1(
                "piplanning-load-test",
                "NO_SECRET",
                resource_owner_key=creds["oauth_token"],
                resource_owner_secret=creds["oauth_token_secret"],
                signature_method=SIGNATURE_RSA,
                rsa_key=private_key,
                signature_type="query",
            )

    @task
    def get_fields(self):
        self.client.get("/rest/api/2/field")

    @task
    def get_10_issues_in_project(self):
        # Choose random project
        projects = self.client.get("/rest/api/2/project").json()
        project = random.choice(projects)
        project_key = project["key"]

        # Query for issues
        search_result = self.client.get(
            "/rest/api/2/search", params={"jql": f"project={project_key}"}
        ).json()
        issues = random.sample(search_result["issues"], 10)
        for issue in issues:
            issue_key = issue["key"]
            _issue = self.client.get(f"/rest/api/2/issue/{issue_key}")
