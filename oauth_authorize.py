import sys
import time
import json
from requests_oauthlib import OAuth1Session
from requests_oauthlib.oauth1_session import SIGNATURE_RSA

try:
    HOST = sys.argv[1]
except Exception:
    print(
        "[ERROR] Please specify a Jira host. e.g: "
        "python oauth_authroze.py https://myjira.net "
    )
    exit()

consumer_key = "piplanning-load-test"
consumer_secret = "piplanning-load-test-secret"

request_token_url = f"{HOST}/plugins/servlet/oauth/request-token"
access_token_url = f"{HOST}/plugins/servlet/oauth/access-token"
authorize_url = f"{HOST}/plugins/servlet/oauth/authorize"

with open("data/certs/jira_privatekey.pem") as f:
    private_key = f.read()

with open("data/certs/jira_publickey.pem") as f:
    public_key = f.read()

# Get request-token
session = OAuth1Session(
    consumer_key,
    client_secret=consumer_secret,
    signature_method=SIGNATURE_RSA,
    rsa_key=private_key,
    signature_type="query",
)
token = session.fetch_request_token(request_token_url)
resource_owner_key = token.get("oauth_token")
resource_owner_secret = token.get("oauth_token_secret")

# Get auth URL
auth_url = session.authorization_url(authorize_url)
print(f"Please visit the following page: {auth_url}")
time.sleep(3)
input("If you have authorized yourself in the browser, press any-key to continue")

# Get access token and save it
session = OAuth1Session(
    consumer_key,
    client_secret=consumer_secret,
    resource_owner_key=resource_owner_key,
    resource_owner_secret=resource_owner_secret,
    signature_method=SIGNATURE_RSA,
    verifier="unused",
    rsa_key=private_key,
    signature_type="query",
)
oauth_tokens = session.fetch_access_token(access_token_url)
with open("data/oauth_creds.json", "w") as f:
    json.dump(oauth_tokens, f, indent=4)
