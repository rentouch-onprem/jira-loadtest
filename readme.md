## What is this for?
Figuring out how Jira behaves under load when using either:
- basic-auth
- session cookie auth
- oauth
as an authentication mechanism.

## Run via docker
Run: `docker run -it -p 8089:8089 -v "${PWD}/data:/home/app/data" harbor3.piplanning.io/pip/jira-loadtest`  

1. Generate cert: (see bellow)
2. Setup Jira (see bellow)
3. `python oauth_authorize.py`
4. Run tests (see bellow)


## How to setup

### install python packages
1. `virtualenv venv`
2. `source venv/bin/activate`
3. `pip install -r requirements.txt`

### Generate certificates for oauth
1. `./generate-cert.sh`

### Setup oAuth connection to Jira

#### Configure Jira
1. Setup a new "application link" in Jira
2. consumer key "piplanning-load-test"
3. consumer name "piplanning.io"
4. public-key: `cat data/certs/jira_publickey.pem`

#### Do oAuth flow to grab keys
1. `python oauth_authorize.py` and follow the instructions


## How to run the tests
Set credentials in env `export PASSWORD=<jira-pw> && export USERNAME=<jira-username>`

#### Run basic-auth test
`AUTH_METHOD=basic locust -f locustfile.py -H <https://your-jira-host>`

#### Run oauth test
`AUTH_METHOD=oauth locust -f locustfile.py -H <https://your-jira-host>`

#### Run cookie test
`AUTH_METHOD=cookie locust -f locustfile.py -H <https://your-jira-host>`