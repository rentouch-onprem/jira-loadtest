CERT_DIR="data/certs"

mkdir -p "$CERT_DIR"
openssl genrsa -out "$CERT_DIR/jira_privatekey.pem" 1024
openssl req -newkey rsa:1024 -x509 -key "$CERT_DIR/jira_privatekey.pem" -out "$CERT_DIR/jira_publickey.cer" -days 365 -subj "/"
openssl pkcs8 -topk8 -nocrypt -in "$CERT_DIR/jira_privatekey.pem" -out "$CERT_DIR/jira_privatekey.pcks8"
openssl x509 -pubkey -noout -in "$CERT_DIR/jira_publickey.cer"  > "$CERT_DIR/jira_publickey.pem"
