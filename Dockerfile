FROM python:3.9

WORKDIR /home/app

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY generate-cert.sh .
COPY oauth_authorize.py .
COPY locustfile.py .

ENTRYPOINT bash